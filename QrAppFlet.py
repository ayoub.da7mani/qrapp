import os
import flet as ft
import qrcode
import qrcode.image.svg

from pyperclip import paste, is_available, PyperclipException
#from pandas import read_clipboard


def main(page: ft.Page) -> None:

  page.title = 'RevoSoft QR Code Gen'
  page.theme_mode = ft.ThemeMode.DARK
  page.horizontal_alignment = ft.CrossAxisAlignment.CENTER
  page.auto_scroll = True
  page.scroll = ft.ScrollMode.HIDDEN

  def paste_content(e: ft.ControlEvent) -> None:
    #get clipboard data
    try:
      if not is_available():
        raise PyperclipException('Not available!')

      tf1.value = paste()

    except PyperclipException as ex:

      raise NotImplementedError(ex) from None

    finally:
      page.update()

  def gen_qr_code(e: ft.ControlEvent) -> None:
    #function that generates QR code
    qr = qrcode.QRCode(version=1,
                       error_correction=qrcode.constants.ERROR_CORRECT_L,
                       box_size=10,
                       border=4,
                       image_factory=qrcode.image.svg.SvgImage)
    qr.add_data(tf1.value)
    qr.make(fit=True)

    qrsvg = qr.make_image(back_color=(255, 195, 235), fill_color=(55, 95, 35))

    qrsvgname = f'qrCodes/qrcode.svg'

    qrsvg.save(qrsvgname)

    t2 = ft.Text('QR Generated')
    try:
    #check if file exists
      if os.path.exists(qrsvgname):
        img = ft.Container(content=ft.Image(
        src=qrsvgname,
        fit=ft.ImageFit.CONTAIN,
    ))
        r3.controls.append(img)
    finally:
      page.update()
      r3.controls.append(t2)

    page.update()

  def textbox_changed(e: ft.ControlEvent) -> None:
    #update generate button color to green

    page.update()

  page.appbar = ft.AppBar(leading=ft.Icon(ft.icons.ALL_INCLUSIVE),
                          title=ft.Text('RevoSoft QR Code Gen'),
                          center_title=True,
                          bgcolor=ft.colors.SURFACE_VARIANT)

  t1 = ft.Image(src='qrCodes/qrcode(ayoub).svg',
                width=500,
                height=500,
                fit=ft.ImageFit.CONTAIN)

  tf1 = ft.TextField(label="QR Code Data",
                     border=ft.InputBorder.NONE,
                     filled=True,
                     hint_text="",
                     on_change=textbox_changed)

  btn1 = ft.IconButton(on_click=paste_content, icon=ft.icons.CONTENT_PASTE)

  r1 = ft.Row([tf1, btn1], alignment=ft.MainAxisAlignment.CENTER)

  btn2 = ft.ElevatedButton("Generate",
                           icon=ft.icons.QR_CODE_2_ROUNDED,
                           on_click=gen_qr_code)

  r2 = ft.Row([btn2], alignment=ft.MainAxisAlignment.CENTER)

  r3 = ft.Row(alignment=ft.MainAxisAlignment.CENTER)

  cl1 = ft.Column([t1, r1, r2, r3], alignment=ft.MainAxisAlignment.CENTER)

  page.add(cl1)

  page.update()


if __name__ == "__main__":
  ft.app(target=main, view=ft.AppView.WEB_BROWSER)
